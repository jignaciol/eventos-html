--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.4
-- Dumped by pg_dump version 9.4.4
-- Started on 2015-06-26 10:00:39 VET

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 7 (class 2615 OID 16398)
-- Name: referencias; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA referencias;


ALTER SCHEMA referencias OWNER TO postgres;

--
-- TOC entry 212 (class 3079 OID 11861)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2252 (class 0 OID 0)
-- Dependencies: 212
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 211 (class 1259 OID 16766)
-- Name: contenido_programatico; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE contenido_programatico (
    id bigint NOT NULL,
    id_programa bigint,
    tema character varying,
    id_ponente bigint,
    hora_inicio timestamp without time zone,
    hora_final timestamp without time zone
);


ALTER TABLE contenido_programatico OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 16764)
-- Name: contenido_programatico_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE contenido_programatico_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE contenido_programatico_id_seq OWNER TO postgres;

--
-- TOC entry 2253 (class 0 OID 0)
-- Dependencies: 210
-- Name: contenido_programatico_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE contenido_programatico_id_seq OWNED BY contenido_programatico.id;


--
-- TOC entry 197 (class 1259 OID 16608)
-- Name: direccion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE direccion (
    id bigint NOT NULL,
    id_pais bigint,
    id_estado bigint,
    id_ciudad bigint,
    localidad_area character varying,
    direccion character varying
);


ALTER TABLE direccion OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 16606)
-- Name: direccion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE direccion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE direccion_id_seq OWNER TO postgres;

--
-- TOC entry 2254 (class 0 OID 0)
-- Dependencies: 196
-- Name: direccion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE direccion_id_seq OWNED BY direccion.id;


--
-- TOC entry 205 (class 1259 OID 16718)
-- Name: evento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE evento (
    id bigint NOT NULL,
    descripcion character varying,
    fecha_inicio date,
    fecha_final date,
    id_direccion bigint,
    id_contacto bigint,
    motivo character varying,
    objetivo character varying
);


ALTER TABLE evento OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 16716)
-- Name: evento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE evento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE evento_id_seq OWNER TO postgres;

--
-- TOC entry 2255 (class 0 OID 0)
-- Dependencies: 204
-- Name: evento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE evento_id_seq OWNED BY evento.id;


--
-- TOC entry 199 (class 1259 OID 16635)
-- Name: informacion_contacto; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE informacion_contacto (
    id bigint NOT NULL,
    id_persona bigint,
    telefono_movil character varying,
    telefono_habitacion character varying,
    email character varying,
    twitter character varying,
    instagram character varying
);


ALTER TABLE informacion_contacto OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 16633)
-- Name: informacion_contacto_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE informacion_contacto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE informacion_contacto_id_seq OWNER TO postgres;

--
-- TOC entry 2256 (class 0 OID 0)
-- Dependencies: 198
-- Name: informacion_contacto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE informacion_contacto_id_seq OWNED BY informacion_contacto.id;


--
-- TOC entry 201 (class 1259 OID 16651)
-- Name: informacion_laboral; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE informacion_laboral (
    id bigint NOT NULL,
    id_persona bigint,
    id_cargo bigint,
    institucion character varying,
    direccion character varying,
    telefono character varying,
    fax character varying,
    email character varying,
    web character varying,
    twitter character varying
);


ALTER TABLE informacion_laboral OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 16649)
-- Name: informacion_laboral_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE informacion_laboral_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE informacion_laboral_id_seq OWNER TO postgres;

--
-- TOC entry 2257 (class 0 OID 0)
-- Dependencies: 200
-- Name: informacion_laboral_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE informacion_laboral_id_seq OWNED BY informacion_laboral.id;


--
-- TOC entry 203 (class 1259 OID 16672)
-- Name: informacion_personal; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE informacion_personal (
    id bigint NOT NULL,
    id_persona bigint,
    id_profesion bigint,
    id_especialidad bigint,
    institucion character varying,
    direccion character varying,
    telefono character varying,
    fax character varying,
    email character varying,
    web character varying,
    twitter character varying
);


ALTER TABLE informacion_personal OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 16670)
-- Name: informacion_personal_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE informacion_personal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE informacion_personal_id_seq OWNER TO postgres;

--
-- TOC entry 2258 (class 0 OID 0)
-- Dependencies: 202
-- Name: informacion_personal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE informacion_personal_id_seq OWNED BY informacion_personal.id;


--
-- TOC entry 189 (class 1259 OID 16507)
-- Name: persona; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE persona (
    id bigint NOT NULL,
    tipo_identidad bigint NOT NULL,
    cedula_passp character varying,
    nombres character varying,
    apellidos character varying,
    fechanac date,
    genero_sexo bigint,
    nacionalidad bigint,
    edo_civil bigint,
    direccion bigint,
    contacto bigint,
    laboral bigint,
    profesion bigint
);


ALTER TABLE persona OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 16503)
-- Name: persona_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE persona_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE persona_id_seq OWNER TO postgres;

--
-- TOC entry 2259 (class 0 OID 0)
-- Dependencies: 187
-- Name: persona_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE persona_id_seq OWNED BY persona.id;


--
-- TOC entry 188 (class 1259 OID 16505)
-- Name: persona_tipo_identidad_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE persona_tipo_identidad_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE persona_tipo_identidad_seq OWNER TO postgres;

--
-- TOC entry 2260 (class 0 OID 0)
-- Dependencies: 188
-- Name: persona_tipo_identidad_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE persona_tipo_identidad_seq OWNED BY persona.tipo_identidad;


--
-- TOC entry 207 (class 1259 OID 16739)
-- Name: programa; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE programa (
    id bigint NOT NULL,
    id_evento bigint,
    id_tipo_programa bigint,
    descripcion character varying,
    fecha_inicio_programa date,
    fecha_final_programa date,
    fecha_inicio_inscripcion date,
    fecha_final_inscripcion date,
    id_direccion bigint,
    id_salon bigint,
    observaciones character varying
);


ALTER TABLE programa OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 16737)
-- Name: programa_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE programa_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE programa_id_seq OWNER TO postgres;

--
-- TOC entry 2261 (class 0 OID 0)
-- Dependencies: 206
-- Name: programa_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE programa_id_seq OWNED BY programa.id;


--
-- TOC entry 209 (class 1259 OID 16755)
-- Name: salon; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE salon (
    id bigint NOT NULL,
    descripcion character varying,
    capacidad integer,
    observaciones character varying
);


ALTER TABLE salon OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 16753)
-- Name: salon_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE salon_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE salon_id_seq OWNER TO postgres;

--
-- TOC entry 2262 (class 0 OID 0)
-- Dependencies: 208
-- Name: salon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE salon_id_seq OWNED BY salon.id;


SET search_path = referencias, pg_catalog;

--
-- TOC entry 186 (class 1259 OID 16494)
-- Name: cargo; Type: TABLE; Schema: referencias; Owner: postgres; Tablespace: 
--

CREATE TABLE cargo (
    id bigint NOT NULL,
    descripcion character varying
);


ALTER TABLE cargo OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 16492)
-- Name: cargo_id_seq; Type: SEQUENCE; Schema: referencias; Owner: postgres
--

CREATE SEQUENCE cargo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cargo_id_seq OWNER TO postgres;

--
-- TOC entry 2263 (class 0 OID 0)
-- Dependencies: 185
-- Name: cargo_id_seq; Type: SEQUENCE OWNED BY; Schema: referencias; Owner: postgres
--

ALTER SEQUENCE cargo_id_seq OWNED BY cargo.id;


--
-- TOC entry 195 (class 1259 OID 16561)
-- Name: ciudad; Type: TABLE; Schema: referencias; Owner: postgres; Tablespace: 
--

CREATE TABLE ciudad (
    id bigint NOT NULL,
    id_pais bigint,
    id_estado bigint,
    descripcion character varying
);


ALTER TABLE ciudad OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 16559)
-- Name: ciudad_id_seq; Type: SEQUENCE; Schema: referencias; Owner: postgres
--

CREATE SEQUENCE ciudad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ciudad_id_seq OWNER TO postgres;

--
-- TOC entry 2264 (class 0 OID 0)
-- Dependencies: 194
-- Name: ciudad_id_seq; Type: SEQUENCE OWNED BY; Schema: referencias; Owner: postgres
--

ALTER SEQUENCE ciudad_id_seq OWNED BY ciudad.id;


--
-- TOC entry 180 (class 1259 OID 16449)
-- Name: edo_civil; Type: TABLE; Schema: referencias; Owner: postgres; Tablespace: 
--

CREATE TABLE edo_civil (
    id bigint NOT NULL,
    descripcion character varying
);


ALTER TABLE edo_civil OWNER TO postgres;

--
-- TOC entry 179 (class 1259 OID 16447)
-- Name: edo_civil_id_seq; Type: SEQUENCE; Schema: referencias; Owner: postgres
--

CREATE SEQUENCE edo_civil_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE edo_civil_id_seq OWNER TO postgres;

--
-- TOC entry 2265 (class 0 OID 0)
-- Dependencies: 179
-- Name: edo_civil_id_seq; Type: SEQUENCE OWNED BY; Schema: referencias; Owner: postgres
--

ALTER SEQUENCE edo_civil_id_seq OWNED BY edo_civil.id;


--
-- TOC entry 184 (class 1259 OID 16480)
-- Name: especialidad; Type: TABLE; Schema: referencias; Owner: postgres; Tablespace: 
--

CREATE TABLE especialidad (
    id bigint NOT NULL,
    descripcion character varying
);


ALTER TABLE especialidad OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 16478)
-- Name: especialidad_id_seq; Type: SEQUENCE; Schema: referencias; Owner: postgres
--

CREATE SEQUENCE especialidad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE especialidad_id_seq OWNER TO postgres;

--
-- TOC entry 2266 (class 0 OID 0)
-- Dependencies: 183
-- Name: especialidad_id_seq; Type: SEQUENCE OWNED BY; Schema: referencias; Owner: postgres
--

ALTER SEQUENCE especialidad_id_seq OWNED BY especialidad.id;


--
-- TOC entry 193 (class 1259 OID 16550)
-- Name: estado; Type: TABLE; Schema: referencias; Owner: postgres; Tablespace: 
--

CREATE TABLE estado (
    id bigint NOT NULL,
    descripcion character varying
);


ALTER TABLE estado OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 16548)
-- Name: estado_id_seq; Type: SEQUENCE; Schema: referencias; Owner: postgres
--

CREATE SEQUENCE estado_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE estado_id_seq OWNER TO postgres;

--
-- TOC entry 2267 (class 0 OID 0)
-- Dependencies: 192
-- Name: estado_id_seq; Type: SEQUENCE OWNED BY; Schema: referencias; Owner: postgres
--

ALTER SEQUENCE estado_id_seq OWNED BY estado.id;


--
-- TOC entry 176 (class 1259 OID 16427)
-- Name: genero_sexo; Type: TABLE; Schema: referencias; Owner: postgres; Tablespace: 
--

CREATE TABLE genero_sexo (
    id bigint NOT NULL,
    descripcion character varying
);


ALTER TABLE genero_sexo OWNER TO postgres;

--
-- TOC entry 175 (class 1259 OID 16425)
-- Name: genero_sexo_id_seq; Type: SEQUENCE; Schema: referencias; Owner: postgres
--

CREATE SEQUENCE genero_sexo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE genero_sexo_id_seq OWNER TO postgres;

--
-- TOC entry 2268 (class 0 OID 0)
-- Dependencies: 175
-- Name: genero_sexo_id_seq; Type: SEQUENCE OWNED BY; Schema: referencias; Owner: postgres
--

ALTER SEQUENCE genero_sexo_id_seq OWNED BY genero_sexo.id;


--
-- TOC entry 178 (class 1259 OID 16438)
-- Name: nacionalidad; Type: TABLE; Schema: referencias; Owner: postgres; Tablespace: 
--

CREATE TABLE nacionalidad (
    id bigint NOT NULL,
    descripcion character varying
);


ALTER TABLE nacionalidad OWNER TO postgres;

--
-- TOC entry 177 (class 1259 OID 16436)
-- Name: nacionalidad_id_seq; Type: SEQUENCE; Schema: referencias; Owner: postgres
--

CREATE SEQUENCE nacionalidad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE nacionalidad_id_seq OWNER TO postgres;

--
-- TOC entry 2269 (class 0 OID 0)
-- Dependencies: 177
-- Name: nacionalidad_id_seq; Type: SEQUENCE OWNED BY; Schema: referencias; Owner: postgres
--

ALTER SEQUENCE nacionalidad_id_seq OWNED BY nacionalidad.id;


--
-- TOC entry 191 (class 1259 OID 16539)
-- Name: pais; Type: TABLE; Schema: referencias; Owner: postgres; Tablespace: 
--

CREATE TABLE pais (
    id bigint NOT NULL,
    descripcion character varying
);


ALTER TABLE pais OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 16537)
-- Name: pais_id_seq; Type: SEQUENCE; Schema: referencias; Owner: postgres
--

CREATE SEQUENCE pais_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pais_id_seq OWNER TO postgres;

--
-- TOC entry 2270 (class 0 OID 0)
-- Dependencies: 190
-- Name: pais_id_seq; Type: SEQUENCE OWNED BY; Schema: referencias; Owner: postgres
--

ALTER SEQUENCE pais_id_seq OWNED BY pais.id;


--
-- TOC entry 182 (class 1259 OID 16460)
-- Name: profesion; Type: TABLE; Schema: referencias; Owner: postgres; Tablespace: 
--

CREATE TABLE profesion (
    id bigint NOT NULL,
    descripcion character varying
);


ALTER TABLE profesion OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 16458)
-- Name: profesion_id_seq; Type: SEQUENCE; Schema: referencias; Owner: postgres
--

CREATE SEQUENCE profesion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE profesion_id_seq OWNER TO postgres;

--
-- TOC entry 2271 (class 0 OID 0)
-- Dependencies: 181
-- Name: profesion_id_seq; Type: SEQUENCE OWNED BY; Schema: referencias; Owner: postgres
--

ALTER SEQUENCE profesion_id_seq OWNED BY profesion.id;


--
-- TOC entry 174 (class 1259 OID 16416)
-- Name: tipo_identidad; Type: TABLE; Schema: referencias; Owner: postgres; Tablespace: 
--

CREATE TABLE tipo_identidad (
    id bigint NOT NULL,
    descripcion character varying
);


ALTER TABLE tipo_identidad OWNER TO postgres;

--
-- TOC entry 173 (class 1259 OID 16414)
-- Name: tipo_identidad_id_seq; Type: SEQUENCE; Schema: referencias; Owner: postgres
--

CREATE SEQUENCE tipo_identidad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tipo_identidad_id_seq OWNER TO postgres;

--
-- TOC entry 2272 (class 0 OID 0)
-- Dependencies: 173
-- Name: tipo_identidad_id_seq; Type: SEQUENCE OWNED BY; Schema: referencias; Owner: postgres
--

ALTER SEQUENCE tipo_identidad_id_seq OWNED BY tipo_identidad.id;


SET search_path = public, pg_catalog;

--
-- TOC entry 2034 (class 2604 OID 16769)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY contenido_programatico ALTER COLUMN id SET DEFAULT nextval('contenido_programatico_id_seq'::regclass);


--
-- TOC entry 2027 (class 2604 OID 16611)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY direccion ALTER COLUMN id SET DEFAULT nextval('direccion_id_seq'::regclass);


--
-- TOC entry 2031 (class 2604 OID 16721)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evento ALTER COLUMN id SET DEFAULT nextval('evento_id_seq'::regclass);


--
-- TOC entry 2028 (class 2604 OID 16638)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY informacion_contacto ALTER COLUMN id SET DEFAULT nextval('informacion_contacto_id_seq'::regclass);


--
-- TOC entry 2029 (class 2604 OID 16654)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY informacion_laboral ALTER COLUMN id SET DEFAULT nextval('informacion_laboral_id_seq'::regclass);


--
-- TOC entry 2030 (class 2604 OID 16675)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY informacion_personal ALTER COLUMN id SET DEFAULT nextval('informacion_personal_id_seq'::regclass);


--
-- TOC entry 2022 (class 2604 OID 16510)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona ALTER COLUMN id SET DEFAULT nextval('persona_id_seq'::regclass);


--
-- TOC entry 2023 (class 2604 OID 16511)
-- Name: tipo_identidad; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona ALTER COLUMN tipo_identidad SET DEFAULT nextval('persona_tipo_identidad_seq'::regclass);


--
-- TOC entry 2032 (class 2604 OID 16742)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY programa ALTER COLUMN id SET DEFAULT nextval('programa_id_seq'::regclass);


--
-- TOC entry 2033 (class 2604 OID 16758)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY salon ALTER COLUMN id SET DEFAULT nextval('salon_id_seq'::regclass);


SET search_path = referencias, pg_catalog;

--
-- TOC entry 2021 (class 2604 OID 16497)
-- Name: id; Type: DEFAULT; Schema: referencias; Owner: postgres
--

ALTER TABLE ONLY cargo ALTER COLUMN id SET DEFAULT nextval('cargo_id_seq'::regclass);


--
-- TOC entry 2026 (class 2604 OID 16564)
-- Name: id; Type: DEFAULT; Schema: referencias; Owner: postgres
--

ALTER TABLE ONLY ciudad ALTER COLUMN id SET DEFAULT nextval('ciudad_id_seq'::regclass);


--
-- TOC entry 2018 (class 2604 OID 16452)
-- Name: id; Type: DEFAULT; Schema: referencias; Owner: postgres
--

ALTER TABLE ONLY edo_civil ALTER COLUMN id SET DEFAULT nextval('edo_civil_id_seq'::regclass);


--
-- TOC entry 2020 (class 2604 OID 16483)
-- Name: id; Type: DEFAULT; Schema: referencias; Owner: postgres
--

ALTER TABLE ONLY especialidad ALTER COLUMN id SET DEFAULT nextval('especialidad_id_seq'::regclass);


--
-- TOC entry 2025 (class 2604 OID 16553)
-- Name: id; Type: DEFAULT; Schema: referencias; Owner: postgres
--

ALTER TABLE ONLY estado ALTER COLUMN id SET DEFAULT nextval('estado_id_seq'::regclass);


--
-- TOC entry 2016 (class 2604 OID 16430)
-- Name: id; Type: DEFAULT; Schema: referencias; Owner: postgres
--

ALTER TABLE ONLY genero_sexo ALTER COLUMN id SET DEFAULT nextval('genero_sexo_id_seq'::regclass);


--
-- TOC entry 2017 (class 2604 OID 16441)
-- Name: id; Type: DEFAULT; Schema: referencias; Owner: postgres
--

ALTER TABLE ONLY nacionalidad ALTER COLUMN id SET DEFAULT nextval('nacionalidad_id_seq'::regclass);


--
-- TOC entry 2024 (class 2604 OID 16542)
-- Name: id; Type: DEFAULT; Schema: referencias; Owner: postgres
--

ALTER TABLE ONLY pais ALTER COLUMN id SET DEFAULT nextval('pais_id_seq'::regclass);


--
-- TOC entry 2019 (class 2604 OID 16463)
-- Name: id; Type: DEFAULT; Schema: referencias; Owner: postgres
--

ALTER TABLE ONLY profesion ALTER COLUMN id SET DEFAULT nextval('profesion_id_seq'::regclass);


--
-- TOC entry 2015 (class 2604 OID 16419)
-- Name: id; Type: DEFAULT; Schema: referencias; Owner: postgres
--

ALTER TABLE ONLY tipo_identidad ALTER COLUMN id SET DEFAULT nextval('tipo_identidad_id_seq'::regclass);


SET search_path = public, pg_catalog;

--
-- TOC entry 2244 (class 0 OID 16766)
-- Dependencies: 211
-- Data for Name: contenido_programatico; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY contenido_programatico (id, id_programa, tema, id_ponente, hora_inicio, hora_final) FROM stdin;
\.


--
-- TOC entry 2273 (class 0 OID 0)
-- Dependencies: 210
-- Name: contenido_programatico_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('contenido_programatico_id_seq', 1, false);


--
-- TOC entry 2230 (class 0 OID 16608)
-- Dependencies: 197
-- Data for Name: direccion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY direccion (id, id_pais, id_estado, id_ciudad, localidad_area, direccion) FROM stdin;
\.


--
-- TOC entry 2274 (class 0 OID 0)
-- Dependencies: 196
-- Name: direccion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('direccion_id_seq', 1, false);


--
-- TOC entry 2238 (class 0 OID 16718)
-- Dependencies: 205
-- Data for Name: evento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY evento (id, descripcion, fecha_inicio, fecha_final, id_direccion, id_contacto, motivo, objetivo) FROM stdin;
\.


--
-- TOC entry 2275 (class 0 OID 0)
-- Dependencies: 204
-- Name: evento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('evento_id_seq', 1, false);


--
-- TOC entry 2232 (class 0 OID 16635)
-- Dependencies: 199
-- Data for Name: informacion_contacto; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY informacion_contacto (id, id_persona, telefono_movil, telefono_habitacion, email, twitter, instagram) FROM stdin;
\.


--
-- TOC entry 2276 (class 0 OID 0)
-- Dependencies: 198
-- Name: informacion_contacto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('informacion_contacto_id_seq', 1, false);


--
-- TOC entry 2234 (class 0 OID 16651)
-- Dependencies: 201
-- Data for Name: informacion_laboral; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY informacion_laboral (id, id_persona, id_cargo, institucion, direccion, telefono, fax, email, web, twitter) FROM stdin;
\.


--
-- TOC entry 2277 (class 0 OID 0)
-- Dependencies: 200
-- Name: informacion_laboral_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('informacion_laboral_id_seq', 1, false);


--
-- TOC entry 2236 (class 0 OID 16672)
-- Dependencies: 203
-- Data for Name: informacion_personal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY informacion_personal (id, id_persona, id_profesion, id_especialidad, institucion, direccion, telefono, fax, email, web, twitter) FROM stdin;
\.


--
-- TOC entry 2278 (class 0 OID 0)
-- Dependencies: 202
-- Name: informacion_personal_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('informacion_personal_id_seq', 1, false);


--
-- TOC entry 2222 (class 0 OID 16507)
-- Dependencies: 189
-- Data for Name: persona; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY persona (id, tipo_identidad, cedula_passp, nombres, apellidos, fechanac, genero_sexo, nacionalidad, edo_civil, direccion, contacto, laboral, profesion) FROM stdin;
\.


--
-- TOC entry 2279 (class 0 OID 0)
-- Dependencies: 187
-- Name: persona_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('persona_id_seq', 1, false);


--
-- TOC entry 2280 (class 0 OID 0)
-- Dependencies: 188
-- Name: persona_tipo_identidad_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('persona_tipo_identidad_seq', 1, false);


--
-- TOC entry 2240 (class 0 OID 16739)
-- Dependencies: 207
-- Data for Name: programa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY programa (id, id_evento, id_tipo_programa, descripcion, fecha_inicio_programa, fecha_final_programa, fecha_inicio_inscripcion, fecha_final_inscripcion, id_direccion, id_salon, observaciones) FROM stdin;
\.


--
-- TOC entry 2281 (class 0 OID 0)
-- Dependencies: 206
-- Name: programa_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('programa_id_seq', 1, false);


--
-- TOC entry 2242 (class 0 OID 16755)
-- Dependencies: 209
-- Data for Name: salon; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY salon (id, descripcion, capacidad, observaciones) FROM stdin;
\.


--
-- TOC entry 2282 (class 0 OID 0)
-- Dependencies: 208
-- Name: salon_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('salon_id_seq', 1, false);


SET search_path = referencias, pg_catalog;

--
-- TOC entry 2219 (class 0 OID 16494)
-- Dependencies: 186
-- Data for Name: cargo; Type: TABLE DATA; Schema: referencias; Owner: postgres
--

COPY cargo (id, descripcion) FROM stdin;
\.


--
-- TOC entry 2283 (class 0 OID 0)
-- Dependencies: 185
-- Name: cargo_id_seq; Type: SEQUENCE SET; Schema: referencias; Owner: postgres
--

SELECT pg_catalog.setval('cargo_id_seq', 1, false);


--
-- TOC entry 2228 (class 0 OID 16561)
-- Dependencies: 195
-- Data for Name: ciudad; Type: TABLE DATA; Schema: referencias; Owner: postgres
--

COPY ciudad (id, id_pais, id_estado, descripcion) FROM stdin;
\.


--
-- TOC entry 2284 (class 0 OID 0)
-- Dependencies: 194
-- Name: ciudad_id_seq; Type: SEQUENCE SET; Schema: referencias; Owner: postgres
--

SELECT pg_catalog.setval('ciudad_id_seq', 1, false);


--
-- TOC entry 2213 (class 0 OID 16449)
-- Dependencies: 180
-- Data for Name: edo_civil; Type: TABLE DATA; Schema: referencias; Owner: postgres
--

COPY edo_civil (id, descripcion) FROM stdin;
\.


--
-- TOC entry 2285 (class 0 OID 0)
-- Dependencies: 179
-- Name: edo_civil_id_seq; Type: SEQUENCE SET; Schema: referencias; Owner: postgres
--

SELECT pg_catalog.setval('edo_civil_id_seq', 1, false);


--
-- TOC entry 2217 (class 0 OID 16480)
-- Dependencies: 184
-- Data for Name: especialidad; Type: TABLE DATA; Schema: referencias; Owner: postgres
--

COPY especialidad (id, descripcion) FROM stdin;
\.


--
-- TOC entry 2286 (class 0 OID 0)
-- Dependencies: 183
-- Name: especialidad_id_seq; Type: SEQUENCE SET; Schema: referencias; Owner: postgres
--

SELECT pg_catalog.setval('especialidad_id_seq', 1, false);


--
-- TOC entry 2226 (class 0 OID 16550)
-- Dependencies: 193
-- Data for Name: estado; Type: TABLE DATA; Schema: referencias; Owner: postgres
--

COPY estado (id, descripcion) FROM stdin;
\.


--
-- TOC entry 2287 (class 0 OID 0)
-- Dependencies: 192
-- Name: estado_id_seq; Type: SEQUENCE SET; Schema: referencias; Owner: postgres
--

SELECT pg_catalog.setval('estado_id_seq', 1, false);


--
-- TOC entry 2209 (class 0 OID 16427)
-- Dependencies: 176
-- Data for Name: genero_sexo; Type: TABLE DATA; Schema: referencias; Owner: postgres
--

COPY genero_sexo (id, descripcion) FROM stdin;
\.


--
-- TOC entry 2288 (class 0 OID 0)
-- Dependencies: 175
-- Name: genero_sexo_id_seq; Type: SEQUENCE SET; Schema: referencias; Owner: postgres
--

SELECT pg_catalog.setval('genero_sexo_id_seq', 1, false);


--
-- TOC entry 2211 (class 0 OID 16438)
-- Dependencies: 178
-- Data for Name: nacionalidad; Type: TABLE DATA; Schema: referencias; Owner: postgres
--

COPY nacionalidad (id, descripcion) FROM stdin;
\.


--
-- TOC entry 2289 (class 0 OID 0)
-- Dependencies: 177
-- Name: nacionalidad_id_seq; Type: SEQUENCE SET; Schema: referencias; Owner: postgres
--

SELECT pg_catalog.setval('nacionalidad_id_seq', 1, false);


--
-- TOC entry 2224 (class 0 OID 16539)
-- Dependencies: 191
-- Data for Name: pais; Type: TABLE DATA; Schema: referencias; Owner: postgres
--

COPY pais (id, descripcion) FROM stdin;
\.


--
-- TOC entry 2290 (class 0 OID 0)
-- Dependencies: 190
-- Name: pais_id_seq; Type: SEQUENCE SET; Schema: referencias; Owner: postgres
--

SELECT pg_catalog.setval('pais_id_seq', 1, false);


--
-- TOC entry 2215 (class 0 OID 16460)
-- Dependencies: 182
-- Data for Name: profesion; Type: TABLE DATA; Schema: referencias; Owner: postgres
--

COPY profesion (id, descripcion) FROM stdin;
\.


--
-- TOC entry 2291 (class 0 OID 0)
-- Dependencies: 181
-- Name: profesion_id_seq; Type: SEQUENCE SET; Schema: referencias; Owner: postgres
--

SELECT pg_catalog.setval('profesion_id_seq', 1, false);


--
-- TOC entry 2207 (class 0 OID 16416)
-- Dependencies: 174
-- Data for Name: tipo_identidad; Type: TABLE DATA; Schema: referencias; Owner: postgres
--

COPY tipo_identidad (id, descripcion) FROM stdin;
\.


--
-- TOC entry 2292 (class 0 OID 0)
-- Dependencies: 173
-- Name: tipo_identidad_id_seq; Type: SEQUENCE SET; Schema: referencias; Owner: postgres
--

SELECT pg_catalog.setval('tipo_identidad_id_seq', 1, false);


SET search_path = public, pg_catalog;

--
-- TOC entry 2072 (class 2606 OID 16774)
-- Name: id_contenido; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY contenido_programatico
    ADD CONSTRAINT id_contenido PRIMARY KEY (id);


--
-- TOC entry 2058 (class 2606 OID 16616)
-- Name: id_direccion; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY direccion
    ADD CONSTRAINT id_direccion PRIMARY KEY (id);


--
-- TOC entry 2066 (class 2606 OID 16726)
-- Name: id_evento; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY evento
    ADD CONSTRAINT id_evento PRIMARY KEY (id);


--
-- TOC entry 2060 (class 2606 OID 16643)
-- Name: id_informacion_contacto; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY informacion_contacto
    ADD CONSTRAINT id_informacion_contacto PRIMARY KEY (id);


--
-- TOC entry 2062 (class 2606 OID 16659)
-- Name: id_informacion_laboral; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY informacion_laboral
    ADD CONSTRAINT id_informacion_laboral PRIMARY KEY (id);


--
-- TOC entry 2064 (class 2606 OID 16680)
-- Name: id_informacion_profesional; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY informacion_personal
    ADD CONSTRAINT id_informacion_profesional PRIMARY KEY (id);


--
-- TOC entry 2050 (class 2606 OID 16516)
-- Name: id_persona; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT id_persona PRIMARY KEY (id);


--
-- TOC entry 2068 (class 2606 OID 16747)
-- Name: id_programa; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY programa
    ADD CONSTRAINT id_programa PRIMARY KEY (id);


--
-- TOC entry 2070 (class 2606 OID 16763)
-- Name: id_salon; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY salon
    ADD CONSTRAINT id_salon PRIMARY KEY (id);


SET search_path = referencias, pg_catalog;

--
-- TOC entry 2048 (class 2606 OID 16502)
-- Name: id_cargo; Type: CONSTRAINT; Schema: referencias; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cargo
    ADD CONSTRAINT id_cargo PRIMARY KEY (id);


--
-- TOC entry 2056 (class 2606 OID 16569)
-- Name: id_ciudad; Type: CONSTRAINT; Schema: referencias; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ciudad
    ADD CONSTRAINT id_ciudad PRIMARY KEY (id);


--
-- TOC entry 2042 (class 2606 OID 16457)
-- Name: id_edo_civil; Type: CONSTRAINT; Schema: referencias; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY edo_civil
    ADD CONSTRAINT id_edo_civil PRIMARY KEY (id);


--
-- TOC entry 2046 (class 2606 OID 16488)
-- Name: id_especialidad; Type: CONSTRAINT; Schema: referencias; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY especialidad
    ADD CONSTRAINT id_especialidad PRIMARY KEY (id);


--
-- TOC entry 2054 (class 2606 OID 16558)
-- Name: id_estado; Type: CONSTRAINT; Schema: referencias; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY estado
    ADD CONSTRAINT id_estado PRIMARY KEY (id);


--
-- TOC entry 2038 (class 2606 OID 16435)
-- Name: id_genero_sexo; Type: CONSTRAINT; Schema: referencias; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY genero_sexo
    ADD CONSTRAINT id_genero_sexo PRIMARY KEY (id);


--
-- TOC entry 2040 (class 2606 OID 16446)
-- Name: id_nacionalidad; Type: CONSTRAINT; Schema: referencias; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY nacionalidad
    ADD CONSTRAINT id_nacionalidad PRIMARY KEY (id);


--
-- TOC entry 2052 (class 2606 OID 16547)
-- Name: id_pais; Type: CONSTRAINT; Schema: referencias; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY pais
    ADD CONSTRAINT id_pais PRIMARY KEY (id);


--
-- TOC entry 2044 (class 2606 OID 16490)
-- Name: id_profesion; Type: CONSTRAINT; Schema: referencias; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY profesion
    ADD CONSTRAINT id_profesion PRIMARY KEY (id);


--
-- TOC entry 2036 (class 2606 OID 16424)
-- Name: id_tipo_identidad; Type: CONSTRAINT; Schema: referencias; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipo_identidad
    ADD CONSTRAINT id_tipo_identidad PRIMARY KEY (id);


SET search_path = public, pg_catalog;

--
-- TOC entry 2088 (class 2606 OID 16665)
-- Name: id_cargo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY informacion_laboral
    ADD CONSTRAINT id_cargo FOREIGN KEY (id_cargo) REFERENCES referencias.cargo(id);


--
-- TOC entry 2083 (class 2606 OID 16617)
-- Name: id_ciudad; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY direccion
    ADD CONSTRAINT id_ciudad FOREIGN KEY (id_ciudad) REFERENCES referencias.ciudad(id);


--
-- TOC entry 2078 (class 2606 OID 16701)
-- Name: id_contacto; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT id_contacto FOREIGN KEY (contacto) REFERENCES informacion_contacto(id);


--
-- TOC entry 2093 (class 2606 OID 16732)
-- Name: id_contacto; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evento
    ADD CONSTRAINT id_contacto FOREIGN KEY (id_contacto) REFERENCES informacion_contacto(id);


--
-- TOC entry 2077 (class 2606 OID 16696)
-- Name: id_direccion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT id_direccion FOREIGN KEY (direccion) REFERENCES direccion(id);


--
-- TOC entry 2092 (class 2606 OID 16727)
-- Name: id_direccion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evento
    ADD CONSTRAINT id_direccion FOREIGN KEY (id_direccion) REFERENCES direccion(id);


--
-- TOC entry 2095 (class 2606 OID 16775)
-- Name: id_direccion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY programa
    ADD CONSTRAINT id_direccion FOREIGN KEY (id_direccion) REFERENCES direccion(id);


--
-- TOC entry 2076 (class 2606 OID 16532)
-- Name: id_edo_civil; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT id_edo_civil FOREIGN KEY (edo_civil) REFERENCES referencias.edo_civil(id);


--
-- TOC entry 2091 (class 2606 OID 16691)
-- Name: id_especialidad; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY informacion_personal
    ADD CONSTRAINT id_especialidad FOREIGN KEY (id_especialidad) REFERENCES referencias.especialidad(id);


--
-- TOC entry 2084 (class 2606 OID 16622)
-- Name: id_estado; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY direccion
    ADD CONSTRAINT id_estado FOREIGN KEY (id_estado) REFERENCES referencias.estado(id);


--
-- TOC entry 2094 (class 2606 OID 16748)
-- Name: id_evento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY programa
    ADD CONSTRAINT id_evento FOREIGN KEY (id_evento) REFERENCES evento(id);


--
-- TOC entry 2074 (class 2606 OID 16522)
-- Name: id_genero_sexo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT id_genero_sexo FOREIGN KEY (genero_sexo) REFERENCES referencias.genero_sexo(id);


--
-- TOC entry 2079 (class 2606 OID 16706)
-- Name: id_informacion_laboral; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT id_informacion_laboral FOREIGN KEY (laboral) REFERENCES informacion_laboral(id);


--
-- TOC entry 2075 (class 2606 OID 16527)
-- Name: id_nacionalidad; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT id_nacionalidad FOREIGN KEY (nacionalidad) REFERENCES referencias.nacionalidad(id);


--
-- TOC entry 2085 (class 2606 OID 16627)
-- Name: id_pais; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY direccion
    ADD CONSTRAINT id_pais FOREIGN KEY (id_pais) REFERENCES referencias.pais(id);


--
-- TOC entry 2086 (class 2606 OID 16644)
-- Name: id_persona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY informacion_contacto
    ADD CONSTRAINT id_persona FOREIGN KEY (id_persona) REFERENCES persona(id);


--
-- TOC entry 2087 (class 2606 OID 16660)
-- Name: id_persona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY informacion_laboral
    ADD CONSTRAINT id_persona FOREIGN KEY (id_persona) REFERENCES persona(id);


--
-- TOC entry 2089 (class 2606 OID 16681)
-- Name: id_persona; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY informacion_personal
    ADD CONSTRAINT id_persona FOREIGN KEY (id_persona) REFERENCES persona(id);


--
-- TOC entry 2090 (class 2606 OID 16686)
-- Name: id_profesion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY informacion_personal
    ADD CONSTRAINT id_profesion FOREIGN KEY (id_profesion) REFERENCES referencias.profesion(id);


--
-- TOC entry 2080 (class 2606 OID 16711)
-- Name: id_profesion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT id_profesion FOREIGN KEY (profesion) REFERENCES referencias.profesion(id);


--
-- TOC entry 2096 (class 2606 OID 16780)
-- Name: id_salon; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY programa
    ADD CONSTRAINT id_salon FOREIGN KEY (id_salon) REFERENCES salon(id);


--
-- TOC entry 2073 (class 2606 OID 16517)
-- Name: id_tipo_identidad; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT id_tipo_identidad FOREIGN KEY (tipo_identidad) REFERENCES referencias.tipo_identidad(id);


SET search_path = referencias, pg_catalog;

--
-- TOC entry 2082 (class 2606 OID 16575)
-- Name: id_estado; Type: FK CONSTRAINT; Schema: referencias; Owner: postgres
--

ALTER TABLE ONLY ciudad
    ADD CONSTRAINT id_estado FOREIGN KEY (id_estado) REFERENCES estado(id);


--
-- TOC entry 2081 (class 2606 OID 16570)
-- Name: id_pais; Type: FK CONSTRAINT; Schema: referencias; Owner: postgres
--

ALTER TABLE ONLY ciudad
    ADD CONSTRAINT id_pais FOREIGN KEY (id_pais) REFERENCES pais(id);


--
-- TOC entry 2251 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2015-06-26 10:00:39 VET

--
-- PostgreSQL database dump complete
--

